/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/

// I made a capitalize function to capitalize the names to be added, updated, removed 
function capitalize(name) {

    // If input is a multiple name that includes spaces
    if (name.includes(' ')) {
        // Convert the string to lowercase and separate each name in a space and store into array
        let tmpNameArr = name.toLowerCase().split(' ');

        // Convert each name in the array to Upper Case
        let upperCasedNameArr = tmpNameArr.map(eachName => {
            return eachName = eachName.charAt(0).toUpperCase() + eachName.slice(1);
        })

        // return the array as a string
        return upperCasedNameArr.join(' ');
    }

    // If input is a single name
    else {

        let tmpName = name;
        return name = name.charAt(0).toUpperCase() + name.toLowerCase().slice(1);
    }
} 

function register(inputUser) {

    // Capitalize the input to accurately check if the registered users includes the input
    let capInputUser = capitalize(inputUser);

    // Check if the input already exists in the registeredUsers
    let doesUserExist = registeredUsers.some(user => user === capInputUser);

    if (doesUserExist) {
        alert('🟥 Registration failed. Username already exists!');
    }

    else {
        registeredUsers.push(capInputUser);
        alert('🟩 Thank you for registering!');
    }
}

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

function addFriend(friendName) {

    // Capitalize the input to accurately check if the registered users includes the input
    let capFriendName = capitalize(friendName);

    // Check if the input friendname already exists in the registeredUsers
    let friendAlreadyRegistered = registeredUsers.some(user => user === capFriendName);

    if (friendAlreadyRegistered) {

        friendsList.push(capFriendName);
        alert(`➕ You have added ${capFriendName} as a friend!`);
    }

    else {

        alert('🟥 User not found!');
    }
}

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
    
function displayFriends() {

    if (friendsList.length === 0) {
        alert('🟥 You currently have 0 friends. Add one first.');
    }

    else {
        friendsList.forEach(friend => {
            console.log(friend);
        });
    }
}

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

function displayNumberOfFriends() {

    if (friendsList.length === 0) {
        alert('🟥 You currently have 0 friends. Add one first.');
    }

    else {
        alert(`📃 You currently have ${friendsList.length} friend(s).`);
    }
}

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

function deleteFriend() {

    if (friendsList.length === 0) {

        alert('🟥 You currently have 0 friends. Add one first.');
    }

    else { friendsList.pop(); };
}

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

function deleteSpecificFriend(deleteFriendName) {

    let capDeleteFriendName = capitalize(deleteFriendName);
    let doesExist = friendsList.some(friend => friend === capDeleteFriendName);

    if (doesExist) {

        let indexOfFriendToDelete = friendsList.indexOf(capDeleteFriendName);

        friendsList.splice(indexOfFriendToDelete, 1);

        alert(`🟩 You have successfully removed ${capDeleteFriendName} from your friends.`);

    }

    else if (friendsList.length === 0) {
        alert('🟥 You currently have 0 friends. Nothing to remove. Add one first.');
    }

    else {
        alert(`⚠ User ${capDeleteFriendName} does not exist from your friend list!`);
    }
}




